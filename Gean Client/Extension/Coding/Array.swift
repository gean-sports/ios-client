//
//  Array.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/19/22.
//

import Foundation

public extension Array where Element: Hashable {

    func uniqued() -> [Element] {

        var seen = Set<Element>()

        return filter { seen.insert($0).inserted }
    }
}
