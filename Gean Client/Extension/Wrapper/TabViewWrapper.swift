//
//  TabViewWrapper.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/9/22.
//

import SwiftUI

struct TabViewWrapper: UIViewControllerRepresentable {

    @Binding var currentTab: MainTabItem

    var controllers: [UIHostingController<AnyView>]

    func makeUIViewController(context: Context) -> UITabBarController {

        let tabBarController = UITabBarController()
        tabBarController.viewControllers = controllers
        tabBarController.tabBar.isHidden = true

        return tabBarController
    }

    func updateUIViewController(_ pageViewController: UITabBarController, context: Context) {

        pageViewController.selectedIndex = currentTab.rawValue
    }
}
