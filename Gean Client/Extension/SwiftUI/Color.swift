//
//  Color.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/8/22.
//

import SwiftUI

extension Color {
    
    enum Interface {

        static let black = Color("interface.black")

        static let cultured = Color("interface.cultured")

        static let platinum = Color("interface.platinum")

        static let white = Color("interface.white")
    }
}
