//
//  View.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/15/22.
//

import SwiftUI

#if canImport(UIKit)

extension View {

    func hideKeyboard() {

        UIApplication.shared.sendAction(
            #selector(UIResponder.resignFirstResponder),
            to: nil,
            from: nil,
            for: nil
        )
    }
}

#endif
