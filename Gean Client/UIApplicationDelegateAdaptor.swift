//
//  UIApplicationDelegateAdaptor.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/21/22.
//

import SwiftUI
import FirebaseCore
import Stripe

final class AppDelegate: NSObject, UIApplicationDelegate {

    // MARK: - Methods

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {

        configureFirebase()

        configureStripe()

        return true
    }

    // MARK: - Configure

    private func configureFirebase() {

        FirebaseApp.configure()
    }

    private func configureStripe() {

        StripeAPI.defaultPublishableKey = "pk_test_51L0k7EKdWiti1dx9x5E3o0Yh6wvil3556XsXppuqbQatIpQPc5EgoeZPhrvZ1nsExUrloByS8qji6YCwq4R1MZZB00Zhnd6Wj5"
    }
}
