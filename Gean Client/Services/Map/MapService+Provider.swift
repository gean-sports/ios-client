//
//  MapService+Provider.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/21/22.
//

import Foundation
import CoreLocation

extension MapService {

    enum Provider: CaseIterable {

        // MARK: - Cases

        case appleMaps
        case googleMaps

        // MARK: - Interaction Methods

        public func getTitle() -> String {

            switch self {
            case .appleMaps:

                return "Apple Maps"
            case .googleMaps:

                return "Google Maps"
            }
        }

        public func getHostURL() -> URL? {

            switch self {
            case .appleMaps:

                return URL(string: "maps://")
            case .googleMaps:

                return URL(string: "comgooglemaps://")
            }
        }

        public func getUrl(_ coordinate: CLLocationCoordinate2D) -> URL? {

            guard let hostUrlRaw = getHostURL()?.absoluteString
            else { return nil }

            let rawUrl: String = {

                switch self {
                case .appleMaps:

                    return String(
                        format: "%@?saddr=&daddr=%f,%f",
                        hostUrlRaw,
                        coordinate.latitude,
                        coordinate.longitude
                    )
                case .googleMaps:

                    return String(
                        format: "%@?saddr=&daddr=%f,%f&directionsmode=driving",
                        hostUrlRaw,
                        coordinate.latitude,
                        coordinate.longitude
                    )
                }
            }()

            return URL(string: rawUrl)
        }
    }
}
