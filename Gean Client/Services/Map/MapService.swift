//
//  MapService.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/21/22.
//

import UIKit
import CoreLocation

final class MapService {

    // MARK: - Interaction Methods
    
    static func canOpen(provider: MapService.Provider) -> Bool {

        guard let url = provider.getHostURL()
        else { return false }

        return UIApplication.shared.canOpenURL(url)
    }

    static func open(provider: MapService.Provider, _ coordinate: CLLocationCoordinate2D) {

        guard let url = provider.getUrl(coordinate)
        else { return }

        UIApplication.shared.open(url)
    }
}
