//
//  ApplePayService.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/22/22.
//

import FirebaseAuth
import Stripe
import PassKit

final class ApplePayService: NSObject {

    // MARK: - Variables

    private var activity: Activity?

    private var applePayCheckedThrowingContinuation: UnsafeContinuation<Void, Error>?

    // MARK: - Interaction Methods

    func pay(for activity: Activity?) async throws {

        guard let activity = activity,
              let price = activity.getPrice(),
              let regionCode = activity.getLocale()?.regionCode,
              let currencyCode = activity.getLocale()?.currencyCode
        else { throw ApplePayError.malformedRequest }

        self.activity = activity

        try await withUnsafeThrowingContinuation { (continuation: UnsafeContinuation<Void, Error>) in

            applePayCheckedThrowingContinuation = continuation

            let paymentRequest = StripeAPI.paymentRequest(
                withMerchantIdentifier: Configuration.Services.Stripe.merchantIdentifier,
                country: regionCode,
                currency: currencyCode
            )

            let decimalNumber = NSDecimalNumber(value: price)
                .rounding(
                    accordingToBehavior: NSDecimalNumberHandler(
                        roundingMode: .plain,
                        scale: 2,
                        raiseOnExactness: false,
                        raiseOnOverflow: false,
                        raiseOnUnderflow: false,
                        raiseOnDivideByZero: true
                    )
                )
            
            paymentRequest.requiredShippingContactFields = []
            paymentRequest.requiredBillingContactFields = []

            paymentRequest.paymentSummaryItems = [
                PKPaymentSummaryItem(
                    label: "Gean Sports",
                    amount: decimalNumber
                )
            ]

            DispatchQueue.main.async {

                let applePayContext = STPApplePayContext(paymentRequest: paymentRequest, delegate: self)
                applePayContext?.presentApplePay()
            }
        }
    }
}

extension ApplePayService: STPApplePayContextDelegate {

    // MARK: - STPApplePayContextDelegate
    
    func applePayContext(_ context: STPApplePayContext, didCreatePaymentMethod paymentMethod: STPPaymentMethod, paymentInformation: PKPayment, completion: @escaping STPIntentClientSecretCompletionBlock) {

        Task {

            do {

                let eventId = (activity as? Event)?.getId()
                let fitnessCenterId = (activity as? FitnessCenter)?.getId()

                guard let userUid = Auth.auth().currentUser?.uid,
                      (eventId != nil || fitnessCenterId != nil)
                else { throw ApplePayError.malformedRequest }

                let paymentIntent = try await NetworkService.createStripePaymentIntent(
                    userUid: userUid,
                    fitnessCenterId: fitnessCenterId,
                    eventId: eventId
                )

                completion(paymentIntent.clientSecret, nil)
            } catch let error {

                completion(nil, error)
            }
        }
    }
    
    func applePayContext(_ context: STPApplePayContext, didCompleteWith status: STPPaymentStatus, error: Error?) {

        switch status {
        case .success:

            applePayCheckedThrowingContinuation?.resume()
        case .userCancellation:

            return
        default:

            let error = error ?? NSError(domain: "com.stripe.apple-pay", code: 404)

            applePayCheckedThrowingContinuation?.resume(throwing: error)
        }
    }
}
