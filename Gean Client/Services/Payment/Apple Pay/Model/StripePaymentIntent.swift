//
//  StripePaymentIntent.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/29/22.
//

import Foundation

final class StripePaymentIntent: Codable {

    // MARK: - Variables

    var id: String

    var clientSecret: String

    // MARK: - Coding Keys

    enum CodingKeys: String, CodingKey {

        case id
        case clientSecret = "client_secret"
    }
}
