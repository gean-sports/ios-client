//
//  ApplePayError.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 6/4/22.
//

import Foundation

enum ApplePayError: Error {

    case malformedRequest
}

extension ApplePayError: LocalizedError {

    public var errorDescription: String? {

        switch self {
        case .malformedRequest:

            return NSLocalizedString(
                "There was some issue with Apple Pay. Try again later.",
                comment: "Some request parameters are missing or invalid."
            )
        }
    }
}
