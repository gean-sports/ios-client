//
//  AuthStatus.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 6/4/22.
//

import Foundation

extension AccountService {

    // MARK: - AuthStatus

    public enum AuthStatus {

        case authenticated, notAuthenticated
    }
}
