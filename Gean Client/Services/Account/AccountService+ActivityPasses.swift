//
//  AccountService+ActivityPasses.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 6/4/22.
//

import Foundation

extension AccountService {

    // MARK: - Interaction Methods

    public func refreshActivityPasses() async throws {

        let activityPassList = try await NetworkService.getActivityPassList()

        await MainActor.run {

            self.activityPassList = activityPassList
        }
    }

    public func appendActivityPass(_ activityPass: ActivityPass) {

        if activityPassList == nil {
            
            activityPassList = [activityPass]
        } else {

            activityPassList?.append(activityPass)
        }
    }
}
