//
//  AccountService.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/22/22.
//

import SwiftUI
import Combine
import FirebaseAuth
import FirebaseService

final class AccountService: ObservableObject {

    // MARK: - Variables

    static let shared = AccountService()

    private var cancellables = Set<AnyCancellable>()

    // MARK: - Binding Variables

    @Published var authStatus: AccountService.AuthStatus?

    @Published var activityPassList: [ActivityPass]?

    // MARK: - Init

    private init() {

        setInitialValues()

        startAuthListener()
    }

    // MARK: - Interaction Methods

    public func signOut() throws {

        try Auth.auth().signOut()

        refreshAccountData()
    }

    // MARK: - Methods

    private func setInitialValues() {

        setAuthStatus(Auth.auth().currentUser)

        refreshAccountData()
    }

    private func refreshAccountData() {

        if authStatus == .authenticated {

            Task(priority: .background) {

                async let refreshActivityPasses: ()? = try? refreshActivityPasses()

                await (refreshActivityPasses)
            }
        } else {

            activityPassList = nil
        }
    }

    private func startAuthListener() {

        let subject = PassthroughSubject<User?, Error>()
        
        Auth.auth().addStateDidChangeListener { (auth, user) in

            subject.send(user)
        }

        subject.sink { _ in } receiveValue: { [weak self] user in

            guard let self = self
            else { return }

            self.setAuthStatus(user)

            self.refreshAccountData()
        }.store(in: &cancellables)
    }

    private func setAuthStatus(_ user: User?) {

        authStatus = user != nil ? .authenticated : .notAuthenticated
    }
}
