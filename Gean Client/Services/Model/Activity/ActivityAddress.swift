//
//  ActivityAddress.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/21/22.
//

import FirebaseFirestore
import CoreLocation

final class ActivityAddress: Codable {

    private var description: String?

    private var coordinate: GeoPoint?
    
    // MARK: - Coding Keys

    enum CodingKeys: String, CodingKey {

        case description, coordinate
    }

    // MARK: - Interaction Methods

    func getDescription() -> String? {

        return description
    }

    func getCoordinate() -> CLLocationCoordinate2D? {

        guard let latitude = coordinate?.latitude,
              let longitude = coordinate?.longitude
        else { return nil }

        return CLLocationCoordinate2D(
            latitude: latitude,
            longitude: longitude
        )
    }
}
//
//extension CLLocationCoordinate2D: Codable {
//
//     public func encode(to encoder: Encoder) throws {
//
//         var container = encoder.unkeyedContainer()
//
//         try container.encode(longitude)
//         try container.encode(latitude)
//     }
//
//     public init(from decoder: Decoder) throws {
//
//         var container = try decoder.unkeyedContainer()
//
//         let longitude = try container.decode(CLLocationDegrees.self)
//         let latitude = try container.decode(CLLocationDegrees.self)
//
//         self.init(latitude: latitude, longitude: longitude)
//     }
// }
