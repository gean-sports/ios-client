//
//  ActivityPass.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/21/22.
//

import Foundation
import FirebaseFirestore

final class ActivityPass: Identifiable, FirestoreObject, Codable {

    // MARK: - Variables

    internal var id: String? {

        return documentId
    }

    private var documentId: String?

    private var fitnessCenter: FitnessCenter?

    private var fitnessCenterReference: DocumentReference?

    private var event: Event?

    private var eventReference: DocumentReference?

    private var endDate: Date?

    // MARK: - Coding Keys

    enum CodingKeys: String, CodingKey {

        case documentId
        case fitnessCenterReference = "fitness_center_reference"
        case eventReference = "event_reference"
        case endDate = "end_date"
    }

    // MARK: - Interaction Methods

    func setDocumentId(_ value: String) {

        documentId = value
    }

    func getQRCodeValue() -> String? {

        return documentId
    }

    func getFitnessCenter() -> FitnessCenter? {

        return fitnessCenter
    }

    func setFitnessCenter(_ value: FitnessCenter) {

        fitnessCenter = value
    }

    func getFitnessCenterReference() -> DocumentReference? {

        return fitnessCenterReference
    }

    func getEvent() -> Event? {

        return event
    }

    func setEvent(_ value: Event) {

        event = value
    }

    func getEventReference() -> DocumentReference? {

        return eventReference
    }

    func getEndDate() -> Date? {

        return endDate
    }
}
