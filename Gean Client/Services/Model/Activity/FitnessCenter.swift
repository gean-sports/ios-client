//
//  FitnessCenter.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/21/22.
//

import Foundation

final class FitnessCenter: Identifiable, FirestoreObject, Activity, Codable {

    // MARK: - Variables

    internal var id: String? {

        return documentId
    }

    private var documentId: String?

    private var name: String?

    private var description: String?

    private var backgroundImageUrl: URL?

    private var address: ActivityAddress?

    private var price: Float?

    private var localeIdentifier: String?

    // MARK: - Coding Keys

    enum CodingKeys: String, CodingKey {

        case documentId, name, description
        case backgroundImageUrl = "background_image"
        case address, price
        case localeIdentifier = "locale_identifier"
    }

    // MARK: - Interaction Methods

    func setDocumentId(_ value: String) {

        documentId = value
    }

    func getId() -> String? {

        return id
    }

    func getName() -> String? {

        return name
    }

    func getDescription() -> String? {

        return description
    }
    
    func getBackgroundImageUrl() -> URL? {

        return backgroundImageUrl
    }
    
    func getAddress() -> ActivityAddress? {

        return address
    }
    
    func getPrice() -> Float? {

        return price
    }

    func getLocale() -> Locale? {

        guard let localeIdentifier = localeIdentifier
        else { return nil }

        return Locale(identifier: localeIdentifier)
    }
}
