//
//  Activity.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/21/22.
//

import Foundation

protocol Activity: AnyObject {

    func getId() -> String?

    func getName() -> String?

    func getDescription() -> String?

    func getBackgroundImageUrl() -> URL?

    func getDate() -> Date?

    func getAddress() -> ActivityAddress?

    func getPrice() -> Float?

    func getLocale() -> Locale?
}

extension Activity {

    func getDate() -> Date? { return nil }

    func getLocalizedDate() -> String? {

        guard let date = getDate()
        else { return nil }

        let dateFormatter = DateFormatter()
        dateFormatter.setLocalizedDateFormatFromTemplate("dLLLjmma")

        return dateFormatter.string(from: date)
    }

    func getLocalizedPrice() -> String? {

        guard let price = getPrice(),
              let locale = getLocale()
        else { return nil }

        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        numberFormatter.locale = locale
        
        return numberFormatter.string(from: NSNumber(value: price))
    }
}
