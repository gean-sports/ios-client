//
//  News.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/22/22.
//

import Foundation
import FirebaseService

final class News: Identifiable, FirestoreObject, Codable {

    internal var id: String? {

        return documentId
    }

    private var documentId: String?

    private var title: String?

    private var description: String?

    private var backgroundImageUrl: URL?

    private var createdAt: Date?

    // MARK: - Coding Keys

    enum CodingKeys: String, CodingKey {

        case title, description
        case backgroundImageUrl = "background_image"
        case createdAt = "created_at"
    }

    // MARK: - Interaction Methods
    
    public func setDocumentId(_ value: String) {

        documentId = value
    }

    public func getTitle() -> String? {

        return title
    }

    public func getDescription() -> String? {

        return description
    }

    public func getBackgroundImageUrl() -> URL? {

        return backgroundImageUrl
    }

    public func getCreatedAt() -> Date? {

        return createdAt
    }
}
