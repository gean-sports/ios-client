//
//  FirestoreQueryItemType.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 6/4/22.
//

import Foundation

public enum FirestoreQueryItemType {

    case isEqualTo,
         isNotEqualTo,
         isLessThan,
         isLessThanOrEqualTo,
         isGreaterThan,
         isGreaterThanOrEqualTo,
         arrayContains,
         arrayContainsAny,
         `in`,
         notIn
}
