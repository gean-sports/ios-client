//
//  AppUrl.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 6/4/22.
//

import Foundation

enum AppUrl {

    static let stripeCreatePaymentIntent = "https://us-central1-gean-sports.cloudfunctions.net/stripeCreatePaymentIntent"
}
