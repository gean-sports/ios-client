//
//  FirestoreQueryLimit.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 6/4/22.
//

import SwiftUI
import FirebaseFirestore

public struct FirestoreQueryLimit {
    
    public var limit: Int
    
    public var orderBy: String
    
    public var descending: Bool
    
    @Binding public var lastDocumentSnapshot: DocumentSnapshot?
    
    public init(
        limit: Int,
        orderBy: String,
        descending: Bool,
        lastDocumentSnapshot: Binding<DocumentSnapshot?>
    ) {
        
        self.limit = limit
        
        self.orderBy = orderBy
        
        self.descending = descending
        
        self._lastDocumentSnapshot = lastDocumentSnapshot
    }
}
