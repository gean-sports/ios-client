//
//  FirestoreObject.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 6/4/22.
//

import Foundation

protocol FirestoreObject {

    func setDocumentId(_ value: String)
}
