//
//  FirestoreQueryItem.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 6/4/22.
//

import Foundation
import FirebaseFirestore

public struct FirestoreQueryItem {

    public let fieldPath: FieldPath?

    public let key: String?

    public let type: FirestoreQueryItemType

    public let values: [Any]
    
    public init(_ fieldPath: FieldPath, _ type: FirestoreQueryItemType, value: Any) {

        self.fieldPath = fieldPath
        self.key = nil
        self.type = type
        self.values = [value]
    }

    public init(_ fieldPath: FieldPath, _ type: FirestoreQueryItemType, values: [Any]) {

        self.fieldPath = fieldPath
        self.key = nil
        self.type = type
        self.values = values
    }

    public init(_ key: String, _ type: FirestoreQueryItemType, value: Any) {

        self.fieldPath = nil
        self.key = key
        self.type = type
        self.values = [value]
    }

    public init(_ key: String, _ type: FirestoreQueryItemType, values: [Any]) {

        self.fieldPath = nil
        self.key = key
        self.type = type
        self.values = values
    }
}
