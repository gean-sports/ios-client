//
//  FirestoreError.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 6/4/22.
//

import Foundation

enum FirestoreError: Error {

    case malformedRequest,
         authRequired
}

extension FirestoreError: LocalizedError {

    public var errorDescription: String? {

        switch self {
        case .malformedRequest:

            return NSLocalizedString(
                "There was some issue. Try again later.",
                comment: "Some request parameters are missing or invalid."
            )
        case .authRequired:

            return NSLocalizedString(
                "You should be signed in.",
                comment: "Authorization is required to fetch this data."
            )
        }
    }
}
