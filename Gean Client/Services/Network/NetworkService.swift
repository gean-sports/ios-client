//
//  NetworkService.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/21/22.
//

import FirebaseFirestore
import FirebaseAuth

final class NetworkService {

    // MARK: - Fitness Centers

    static public func getFitnessCenters(searchQuery: String? = nil, limit: Int = 15) async throws -> [FitnessCenter] {

        var queryItems = [FirestoreQueryItem]()

        if let searchQuery = searchQuery {

            queryItems += [
                FirestoreQueryItem("name", .isGreaterThanOrEqualTo, value: searchQuery),
                FirestoreQueryItem("name", .isLessThanOrEqualTo, value: searchQuery + "~")
            ]
        }

        return try await fetchCollectionFromFirestore(
            path: "fitness_centers",
            queryLimit: FirestoreQueryLimit(
                limit: limit,
                orderBy: FitnessCenter.CodingKeys.name.stringValue,
                descending: true,
                lastDocumentSnapshot: .constant(nil)
            ),
            queryItems: queryItems
        )
    }

    // MARK: - Events

    static public func getEvents(searchQuery: String? = nil, limit: Int = 15) async throws -> [Event] {

        if let searchQuery = searchQuery {

            return try await fetchCollectionFromFirestore(
                path: "events",
                queryLimit: FirestoreQueryLimit(
                    limit: limit,
                    orderBy: Event.CodingKeys.name.stringValue,
                    descending: false,
                    lastDocumentSnapshot: .constant(nil)
                ),
                queryItems: [
                    FirestoreQueryItem("name", .isGreaterThanOrEqualTo, value: searchQuery),
                    FirestoreQueryItem("name", .isLessThanOrEqualTo, value: searchQuery + "~")
                ]
            )
        } else {

            return try await fetchCollectionFromFirestore(
                path: "events",
                queryLimit: FirestoreQueryLimit(
                    limit: limit,
                    orderBy: Event.CodingKeys.date.stringValue,
                    descending: true,
                    lastDocumentSnapshot: .constant(nil)
                ),
                queryItems: [
                    FirestoreQueryItem("date", .isGreaterThanOrEqualTo, value: Date.now)
                ]
            )
        }
    }

    // MARK: - News

    static public func getNews() async throws -> [News] {

        return try await fetchCollectionFromFirestore(
            path: "news",
            queryLimit: FirestoreQueryLimit(
                limit: 15,
                orderBy: News.CodingKeys.createdAt.stringValue,
                descending: true,
                lastDocumentSnapshot: .constant(nil)
            )
        )
    }

    // MARK: - Passes

    static public func getActivityPassList() async throws -> [ActivityPass] {

        guard let userUid = Auth.auth().currentUser?.uid
        else { throw FirestoreError.authRequired }

        let reference = Firestore
            .firestore()
            .collection("users")
            .document(userUid)

        let activityPassCollection = try await fetchCollectionFromFirestore(
            path: "passes",
            queryItems: [
                FirestoreQueryItem("user_reference", .isEqualTo, value: reference),
                FirestoreQueryItem("end_date", .isGreaterThan, value: Date.now)
            ]
        ) as [ActivityPass]

        let fitnessCenterQueryItems: [FirestoreQueryItem] = activityPassCollection.compactMap {

            guard let documentId = $0.getFitnessCenterReference()?.documentID
            else { return nil }

            return FirestoreQueryItem(FieldPath.documentID(), .isEqualTo, value: documentId)
        }

        let fitnessCenterReferences = activityPassCollection
            .compactMap { $0.getFitnessCenterReference() }

        let eventReferences = activityPassCollection
            .compactMap { $0.getEventReference() }

        if !fitnessCenterQueryItems.isEmpty {

            let fitnessCenterCollection = try await fetchCollectionFromFirestore(
                path: "fitness_centers",
                queryItems: [
                    FirestoreQueryItem(FieldPath.documentID(), .in, values: fitnessCenterReferences)
                ]
            ) as [FitnessCenter]

            for activityPass in activityPassCollection {

                guard let fitnessCenterId = activityPass.getFitnessCenterReference()?.documentID,
                      let fitnessCenter = fitnessCenterCollection.first(where: { $0.getId() == fitnessCenterId })
                else { continue }

                activityPass.setFitnessCenter(fitnessCenter)
            }
        }

        if !eventReferences.isEmpty {

            let eventCollection = try await fetchCollectionFromFirestore(
                path: "events",
                queryItems: [
                    FirestoreQueryItem(FieldPath.documentID(), .in, values: eventReferences)
                ]
            ) as [Event]

            for activityPass in activityPassCollection {

                guard let eventId = activityPass.getEventReference()?.documentID,
                      let event = eventCollection.first(where: { $0.getId() == eventId })
                else { continue }

                activityPass.setEvent(event)
            }
        }

        return activityPassCollection
    }

    static public func getActivityPass(fitnessCenterId: String? = nil, eventId: String? = nil) async throws -> ActivityPass? {

        let queryItem: FirestoreQueryItem? = {

            if let fitnessCenterId = fitnessCenterId {

                let reference = Firestore
                    .firestore()
                    .collection("fitness_centers")
                    .document(fitnessCenterId)

                return FirestoreQueryItem("fitness_center_reference", .isEqualTo, value: reference)
            } else if let eventId = eventId {

                let reference = Firestore
                    .firestore()
                    .collection("events")
                    .document(eventId)

                return FirestoreQueryItem("event_reference", .isEqualTo, value: reference)
            } else {

                return nil
            }
        }()

        guard let queryItem = queryItem,
              let userUid = Auth.auth().currentUser?.uid
        else { throw FirestoreError.malformedRequest }
        
        let userReference = Firestore
            .firestore()
            .collection("users")
            .document(userUid)

        let collection = try await fetchCollectionFromFirestore(
            path: "passes",
            queryItems: [
                queryItem,
                FirestoreQueryItem("user_reference", .isEqualTo, value: userReference)
            ]
        ) as [ActivityPass]

        if let pass = collection.first {

            if let fitnessCenterReference = pass.getFitnessCenterReference() {

                let fitnessCenter = try await fitnessCenterReference.getDocument().data(as: FitnessCenter.self)
                fitnessCenter.setDocumentId(fitnessCenterReference.documentID)

                pass.setFitnessCenter(fitnessCenter)
            } else if let eventReference = pass.getEventReference() {

                let event = try await eventReference.getDocument().data(as: Event.self)
                event.setDocumentId(eventReference.documentID)

                pass.setEvent(event)
            }

            return pass
        } else {

            return nil
        }
    }

    // MARK: - Stripe

    static public func createStripePaymentIntent(userUid: String, fitnessCenterId: String?, eventId: String?) async throws -> StripePaymentIntent {

        guard var urlComponents = URLComponents(string: AppUrl.stripeCreatePaymentIntent)
        else { throw ApplePayError.malformedRequest }

        urlComponents.queryItems = [
            URLQueryItem(name: "user_uid", value: userUid)
        ]

        if let fitnessCenterId = fitnessCenterId {

            let queryItem = URLQueryItem(name: "fitness_center_id", value: fitnessCenterId)

            urlComponents.queryItems?.append(queryItem)
        }

        if let eventId = eventId {

            let queryItem = URLQueryItem(name: "event_id", value: eventId)

            urlComponents.queryItems?.append(queryItem)
        }

        guard let url = urlComponents.url
        else { throw ApplePayError.malformedRequest }

        let (data, _) = try await URLSession.shared.data(from: url)
        return try JSONDecoder().decode(StripePaymentIntent.self, from: data)
    }

    // MARK: - Methods

    static private func fetchCollectionFromFirestore<T: FirestoreObject & Decodable>(path: String, queryLimit: FirestoreQueryLimit? = nil, queryItems: [FirestoreQueryItem] = []) async throws -> [T] {

        var query: Query = Firestore
            .firestore()
            .collection(path)

        if let queryLimit = queryLimit {

            query = query
                .limit(to: queryLimit.limit)
                .order(
                    by: queryLimit.orderBy,
                    descending: queryLimit.descending
                )
        }

        for queryItem in queryItems {

            guard let value = queryItem.values.first
            else { continue }

            switch queryItem.type {
            case .isEqualTo:

                if let key = queryItem.key {

                    query = query.whereField(key, isEqualTo: value)
                } else if let fieldPath = queryItem.fieldPath {

                    query = query.whereField(fieldPath, isEqualTo: value)
                }
            case .isNotEqualTo:

                if let key = queryItem.key {

                    query = query.whereField(key, isNotEqualTo: value)
                } else if let fieldPath = queryItem.fieldPath {

                    query = query.whereField(fieldPath, isNotEqualTo: value)
                }
            case .isLessThan:

                if let key = queryItem.key {

                    query = query.whereField(key, isLessThan: value)
                } else if let fieldPath = queryItem.fieldPath {

                    query = query.whereField(fieldPath, isLessThan: value)
                }
            case .isLessThanOrEqualTo:

                if let key = queryItem.key {

                    query = query.whereField(key, isLessThanOrEqualTo: value)
                } else if let fieldPath = queryItem.fieldPath {

                    query = query.whereField(fieldPath, isLessThanOrEqualTo: value)
                }
            case .isGreaterThan:

                if let key = queryItem.key {

                    query = query.whereField(key, isGreaterThan: value)
                } else if let fieldPath = queryItem.fieldPath {

                    query = query.whereField(fieldPath, isGreaterThan: value)
                }
            case .isGreaterThanOrEqualTo:

                if let key = queryItem.key {

                    query = query.whereField(key, isGreaterThanOrEqualTo: value)
                } else if let fieldPath = queryItem.fieldPath {

                    query = query.whereField(fieldPath, isGreaterThanOrEqualTo: value)
                }
            case .arrayContains:

                if let key = queryItem.key {

                    query = query.whereField(key, arrayContains: queryItem.values)
                } else if let fieldPath = queryItem.fieldPath {

                    query = query.whereField(fieldPath, arrayContains: queryItem.values)
                }
            case .arrayContainsAny:

                if let key = queryItem.key {

                    query = query.whereField(key, arrayContainsAny: queryItem.values)
                } else if let fieldPath = queryItem.fieldPath {

                    query = query.whereField(fieldPath, arrayContainsAny: queryItem.values)
                }
            case .in:

                if let key = queryItem.key {

                    query = query.whereField(key, in: queryItem.values)
                } else if let fieldPath = queryItem.fieldPath {

                    query = query.whereField(fieldPath, in: queryItem.values)
                }
            case .notIn:

                if let key = queryItem.key {

                    query = query.whereField(key, notIn: queryItem.values)
                } else if let fieldPath = queryItem.fieldPath {

                    query = query.whereField(fieldPath, notIn: queryItem.values)
                }
            }
        }

        return try await query.getDocuments().documents.compactMap { document in

            guard let data = try? document.data(as: T.self)
            else { return nil }

            data.setDocumentId(document.documentID)

            return data
        }
    }
}
