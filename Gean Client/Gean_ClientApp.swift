//
//  Gean_ClientApp.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/7/22.
//

import SwiftUI

@main
struct Gean_ClientApp: App {

    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate

    var body: some Scene {

        WindowGroup {

            MainTabView()
        }
    }
}
