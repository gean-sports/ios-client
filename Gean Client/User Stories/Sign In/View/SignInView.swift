//
//  SignInView.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/15/22.
//

import SwiftUI
import AuthenticationServices
import FirebaseService
import FirebaseAuth

struct SignInView: View {

    // MARK: - Variables

    @Environment(\.dismiss) var dismiss

    @State private var isAlertShown: Bool = false
    @State private var alertMessage: String?

    // MARK: - Body

    var body: some View {

        ZStack {

            Color.Interface.black
                .ignoresSafeArea()

            VStack(spacing: 30) {

                HStack {
                    
                    Spacer()
                    
                    Button {

                        dismiss()
                    } label: {

                        Image(systemName: "xmark.circle.fill")
                            .resizable()
                            .foregroundColor(Color.Interface.white)
                            .frame(width: 30, height: 30)
                    }
                    .padding(10)
                }

                
                Spacer()

                Image("logo.medium")
                    .resizable()
                    .frame(width: 100, height: 100)

                Spacer()

                Spacer()
                
                Text("Sign in to unlock all features")
                    .font(.system(size: 20, weight: .bold, design: .rounded))
                    .foregroundColor(Color.Interface.white)
                    .multilineTextAlignment(.center)
                    .frame(maxWidth: .infinity)

                FirebaseSignInWithAppleButton(
                    label: .continue,
                    requestedScopes: [.fullName, .email]
                ) { result in

                    switch result {
                    case .success:

                        dismiss()
                    case .failure(let error):

                        guard error._domain == AuthErrorDomain
                        else { return }

                        isAlertShown = true
                        alertMessage = error.localizedDescription
                    }
                }
                .signInWithAppleButtonStyle(.white)
                .clipShape(Capsule())
                .frame(height: 50)
                .frame(maxWidth: 400)
            }
            .padding(20)
        }
        .alert("Something went wrong", isPresented: $isAlertShown, actions: {
            
        }, message: {
            Text(alertMessage ?? "Try again, please")
                .onAppear { alertMessage = nil }
        })
    }
}

struct SignInView_Previews: PreviewProvider {
    static var previews: some View {
        SignInView()
            .environmentObject(AuthState())
    }
}
