//
//  MainTabCoordinator.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/19/22.
//

import SwiftUI

final class MainTabCoordinator: ObservableObject {

    @Published var isShowingSignInView: Bool = false

    @Published var isShowingSearchViewWithConfiguration: SearchViewConfiguration?

    @Published var isShowindNewsDetailsViewWithConfiguration: NewsDetailsViewConfiguration?

    @Published var isShowingActivityDetailViewWithConfiguration: ActivityDetailViewConfiguration?
}
