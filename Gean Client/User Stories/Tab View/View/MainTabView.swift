//
//  MainTabView.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/8/22.
//

import SwiftUI
import FirebaseService

struct MainTabView: View {

    // MARK: - Variables

    @State private var selectedTab = MainTabItem.home

    @ObservedObject private var mainTabCoordinator = MainTabCoordinator()

    // MARK: - Body

    var body: some View {

        ZStack(alignment: .bottom) {

            TabViewWrapper(
                currentTab: $selectedTab,
                controllers: MainTabItem.allCases.map {
                    UIHostingController(
                        rootView: AnyView(
                            $0.getView()
                                .environmentObject(mainTabCoordinator)
                        )
                    )
                }
            )
            .ignoresSafeArea(.all, edges: .bottom)
            .background(Color.Interface.white)

            VStack {

                Spacer()

                MainTabBar(selectedTab: $selectedTab)
            }
            .padding(.bottom, 8)
            .ignoresSafeArea(.keyboard)
        }
        .sheet(
            isPresented: $mainTabCoordinator.isShowingSignInView,
            onDismiss: { mainTabCoordinator.isShowingSignInView = false },
            content: {
                
                SignInView()
            }
        )
        .sheet(
            item: $mainTabCoordinator.isShowingSearchViewWithConfiguration,
            onDismiss: { mainTabCoordinator.isShowingSearchViewWithConfiguration = nil },
            content: { configuration in

                SearchView(viewModel: SearchView.ViewModel(configuration: configuration))
            }
        )
        .sheet(
            item: $mainTabCoordinator.isShowindNewsDetailsViewWithConfiguration,
            onDismiss: { mainTabCoordinator.isShowindNewsDetailsViewWithConfiguration = nil },
            content: { item in
                
                NewsDetailsView(news: item.news)
            }
        )
        .sheet(
            item: $mainTabCoordinator.isShowingActivityDetailViewWithConfiguration,
            onDismiss: { mainTabCoordinator.isShowingActivityDetailViewWithConfiguration = nil },
            content: { _ in
                
                ActivityDetailView(
                    viewModel: ActivityDetailView.ViewModel(
                        mainTabCoordinator
                            .isShowingActivityDetailViewWithConfiguration?
                            .activity
                    )
                )
            }
        )
    }
}

struct MainTabBar_Previews: PreviewProvider {
    static var previews: some View {
        MainTabView()
    }
}
