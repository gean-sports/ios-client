//
//  MainTabBarItem.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/8/22.
//

import SwiftUI

struct MainTabBarItem: View {

    // MARK: - Variables
    
    @Binding var selectedTab: MainTabItem
    
    var tabItem: MainTabItem
    
    // MARK: - Body

    var body: some View {

        Button {

            selectedTab = tabItem
        } label: {

            let tintColor = selectedTab == tabItem ? Color.Interface.black : Color.Interface.platinum

            tabItem.getButtonImage()
                .resizable()
                .scaledToFit()
                .frame(width: 30, height: 30)
                .tint(tintColor)
        }
        .padding(10)
    }
}
