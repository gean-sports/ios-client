//
//  MainTabBar.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/8/22.
//

import SwiftUI

struct MainTabBar: View {

    // MARK: - Variables
    
    @Binding var selectedTab: MainTabItem
        
    // MARK: - Body

    var body: some View {

        HStack(spacing: 10) {

            ForEach(MainTabItem.allCases, id: \.rawValue) { tabItem in

                MainTabBarItem(
                    selectedTab: $selectedTab,
                    tabItem: tabItem
                )
            }
        }
        .padding([.leading, .trailing], 15)
        .background(Color.Interface.white)
        .cornerRadius(.infinity)
        .shadow(color: Color.Interface.black.opacity(0.3), radius: 5)
    }
}
