//
//  MainTabItem.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/8/22.
//

import SwiftUI

enum MainTabItem: Int, CaseIterable {

    // MARK: Cases

    case home = 0
    case myPasses

    // MARK: - Interaction Methods

    func getButtonImage() -> Image {
        switch self {
        case .home:

            return Image(systemName: "globe.europe.africa.fill")
        case .myPasses:

            return Image(systemName: "square.stack.fill")
        }
    }
    
    @ViewBuilder
    func getView() -> some View {

        switch self {
        case .home:

            HomeView()
        case .myPasses:

            let configuration = SearchViewConfiguration(
                scope: [.userFitnessCenters, .userEvents],
                isShowingSearchBar: false,
                isLocationEnabled: false,
                isShowingTabBarPadding: true
            )

            SearchView(viewModel: SearchView.ViewModel(configuration: configuration))
        }
    }
}
