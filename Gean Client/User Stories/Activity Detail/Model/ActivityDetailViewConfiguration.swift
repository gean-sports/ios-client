//
//  ActivityDetailViewConfiguration.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/19/22.
//

import Foundation

struct ActivityDetailViewConfiguration: Identifiable {
    
    // MARK: - Variables
    
    var id: Int { 0 }
    
    var activity: Activity
}
