//
//  ActivityDetailView+ViewModel.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/21/22.
//

import SwiftUI
import Combine

extension ActivityDetailView {

    @MainActor
    final class ViewModel: ObservableObject {

        // MARK: - Variables

        @Published var activity: Activity?

        @Published var activityPass: ActivityPass?

        @Published var isFetchingActivityPass: Bool = false

        private var applePayService = ApplePayService()

        private var cancellables = Set<AnyCancellable>()

        // MARK: - Init

        init(_ activity: Activity?) {

            self.activity = activity

            self.activityPass = getLocalActivityPass()
            
            bindActivityPassList()
        }

        // MARK: - Interaction Methods

        public func performPayment() {

            Task(priority: .userInitiated) {

                do {

                    try await applePayService.pay(for: activity)

                    await monitorActivityPass()
                } catch let error {

                    print(error.localizedDescription)
                }
            }
        }

        // MARK: - Methods

        private func bindActivityPassList() {

            AccountService.shared.$activityPassList.sink { [weak self] activityPassList in

                guard let self = self
                else { return }

                self.activityPass = self.getLocalActivityPass(from: activityPassList)
            }.store(in: &cancellables)
        }

        private func monitorActivityPass() async {

            let activityPass = try? await NetworkService.getActivityPass(
                fitnessCenterId: (activity as? FitnessCenter)?.getId(),
                eventId: (activity as? Event)?.getId()
            )

            if let activityPass = activityPass {

                AccountService.shared.appendActivityPass(activityPass)

                await MainActor.run {

                    self.activityPass = activityPass
                }
            } else {

                try? await Task.sleep(nanoseconds: 1_000_000_000)

                await monitorActivityPass()
            }
        }

        private func getLocalActivityPass(from activityPassList: [ActivityPass]? = nil) -> ActivityPass? {

            guard let activity = activity
            else { return nil }

            return (activityPassList ?? AccountService.shared.activityPassList)?.first { activityPass in

                let fitnessCenterId = activityPass.getFitnessCenter()?.getId()
                let eventId = activityPass.getEvent()?.getId()

                if let thisFitnessCenterId = (activity as? FitnessCenter)?.getId() {

                    return thisFitnessCenterId == fitnessCenterId
                } else if let thisEventId = (activity as? Event)?.getId() {

                    return thisEventId == eventId
                } else {

                    return false
                }
            }
        }
    }
}
