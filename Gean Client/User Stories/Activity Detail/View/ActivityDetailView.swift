//
//  DetailView.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/15/22.
//

import SwiftUI
import NukeUI
import MapKit
import CoreImage.CIFilterBuiltins
import StripeUICore

struct ActivityDetailView: View {

    // MARK: - Variables

    @Environment(\.presentationMode) var presentationMode

    @StateObject var viewModel: ViewModel

    // MARK: - Body

    var body: some View {

        ScrollView(
            .vertical,
            showsIndicators: false
        ) {

            VStack(
                alignment: .leading,
                spacing: 25
            ) {

                ActivityDetailHeader(viewModel: viewModel)
                    .frame(height: 200)

                Group {

                    if viewModel.activityPass == nil {

                        ActivityDetailGetAccessSection(viewModel: viewModel)
                    } else {

                        ActivityDetailQRCodeSection(viewModel: viewModel)
                    }

                    if let description = viewModel.activity?.getDescription() {

                        ActivityDetailDescriptionSection(decription: description)
                    }

                    if let address = viewModel.activity?.getAddress() {

                        ActivityDetailMapSection(address: address)
                    }
                }
                .padding([.horizontal], 20)
            }
            .animation(.easeInOut, value: (viewModel.activityPass == nil))
        }
        .edgesIgnoringSafeArea(.top)
    }
}

struct ActivityDetailView_Previews: PreviewProvider {
    static var previews: some View {
        ActivityDetailView(viewModel: ActivityDetailView.ViewModel(nil))
    }
}
