//
//  ActivityDetailDescriptionSection.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/21/22.
//

import SwiftUI

struct ActivityDetailDescriptionSection: View {

    // MARK: - Variables

    var decription: String

    // MARK: - Body

    var body: some View {

        VStack(alignment: .leading, spacing: 15) {

            Text("Description")
                .font(.system(size: 25, weight: .bold))
                .foregroundColor(Color.Interface.black)

            Text(decription)
                .font(.system(size: 16, weight: .regular))
                .foregroundColor(Color.Interface.black)
        }
    }
}

struct ActivityDetailDescriptionSection_Previews: PreviewProvider {
    static var previews: some View {
        ActivityDetailDescriptionSection(decription: "")
    }
}
