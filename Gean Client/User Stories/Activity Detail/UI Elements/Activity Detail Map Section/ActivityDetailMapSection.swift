//
//  ActivityDetailMapSection.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/21/22.
//

import SwiftUI
import CoreLocation
import MapKit
import UniformTypeIdentifiers

struct ActivityDetailMapSection: View {

    // MARK: - Variables

    var address: ActivityAddress?

    @State private var mapSnapshotImage: UIImage?

    @State private var isSeeOnMapActionsShown = false

    // MARK: - Body

    var body: some View {

        VStack(alignment: .leading, spacing: 15) {

            VStack(alignment: .leading, spacing: 10) {

                Text("Where to find")
                    .font(.system(size: 25, weight: .bold))
                    .foregroundColor(Color.Interface.black)

                if let addressDescription = address?.getDescription() {

                    Text(addressDescription)
                        .font(.system(size: 16, weight: .bold))
                        .foregroundColor(Color.Interface.platinum)
                        .lineLimit(1)
                }
            }

            Button {

                isSeeOnMapActionsShown = true
            } label: {

                ZStack {

                    Color.Interface.cultured

                    GeometryReader { proxy in

                            Image(uiImage: mapSnapshotImage ?? UIImage())
                                .animation(.none)
                                .task {

                                    if let coordinate = address?.getCoordinate() {

                                        generateMapImage(
                                            coordinate: coordinate,
                                            imageSize: CGSize(
                                                width: proxy.size.width,
                                                height: proxy.size.height
                                            )
                                        )
                                    }
                                }
                    }
                    .opacity(mapSnapshotImage == nil ? 0 : 1)
                    .animation(.easeInOut, value: mapSnapshotImage)
                }
            }
            .frame(height: 150)
            .confirmationDialog(
                "",
                isPresented: $isSeeOnMapActionsShown,
                titleVisibility: .hidden
            ) {

                let providers = MapService.Provider.allCases
                    .filter { MapService.canOpen(provider: $0) }

                ForEach(providers, id: \.hashValue) { provider in

                    if
                        let coordinate = address?.getCoordinate(),
                        MapService.canOpen(provider: provider)
                    {

                        let buttonTitle = String(
                            format: "See on %@",
                            provider.getTitle()
                        )

                        Button(buttonTitle) {

                            MapService.open(
                                provider: provider,
                                coordinate
                            )
                        }
                    }
                }

                if let addressDescription = address?.getDescription() {

                    Button("Copy address") {

                        UIPasteboard.general.setValue(
                            addressDescription,
                            forPasteboardType: UTType.plainText.identifier
                        )
                    }
                }
            }
        }
    }

    private func generateMapImage(coordinate: CLLocationCoordinate2D, imageSize: CGSize) {

        let span = CLLocationDegrees(0.001)

        let region = MKCoordinateRegion(
            center: coordinate,
            span: MKCoordinateSpan(
                latitudeDelta: span,
                longitudeDelta: span
            )
        )

        let options = MKMapSnapshotter.Options()
        options.region = region
        options.size = imageSize
        options.showsBuildings = true

        let snapshotter = MKMapSnapshotter(options: options)
        snapshotter.start { (snapshot, _) in

            guard let snapshot = snapshot
            else { return }

            let mapImage = snapshot.image

            let annotationView = MKMarkerAnnotationView(annotation: nil, reuseIdentifier: nil)
            annotationView.contentMode = .scaleAspectFit
            annotationView.bounds = CGRect(
                x: 0,
                y: 0,
                width: 40,
                height: 40
            )

            UIGraphicsBeginImageContextWithOptions(mapImage.size, true, mapImage.scale)

            mapImage.draw(at: CGPoint.zero)

            annotationView.drawHierarchy(
                in: CGRect(
                    origin: CGPoint(
                        x: (imageSize.width / 2) - (annotationView.bounds.width / 2),
                        y: (imageSize.height / 2) - (annotationView.bounds.height / 2)
                    ),
                    size: annotationView.bounds.size
                ),
                afterScreenUpdates: true
            )

            let compositeImage = UIGraphicsGetImageFromCurrentImageContext()

            UIGraphicsEndImageContext()

            mapSnapshotImage = compositeImage
        }
    }
}

struct ActivityDetailMapSection_Previews: PreviewProvider {
    static var previews: some View {
        ActivityDetailMapSection(address: nil)
    }
}
