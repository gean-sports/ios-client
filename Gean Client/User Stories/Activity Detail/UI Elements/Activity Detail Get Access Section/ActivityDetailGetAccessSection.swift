//
//  ActivityDetailGetAccessSection.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/21/22.
//

import SwiftUI

struct ActivityDetailGetAccessSection: View {

    // MARK: - Variables

    @ObservedObject var viewModel: ActivityDetailView.ViewModel

    @State private var isShowingSignInPage: Bool = false

    // MARK: - Body

    var body: some View {

        Button {

            if AccountService.shared.authStatus == .authenticated {

                viewModel.performPayment()
            } else {

                isShowingSignInPage = true
            }
        } label: {

            Text(getButtonTitle())
                .lineLimit(1)
                .font(.system(size: 16, weight: .bold))
                .truncationMode(.middle)
                .foregroundColor(Color.Interface.white)
                .padding(.vertical, 13)
                .padding(.horizontal, 20)
                .frame(maxWidth: .infinity, minHeight: 50)
                .background(Color.Interface.black)
                .foregroundColor(Color.Interface.white)
        }
        .sheet(isPresented: $isShowingSignInPage) {

            SignInView()
        }
    }

    // MARK: - Methods

    private func getButtonTitle() -> String {

        return [
            "Get access",
            viewModel.activity?.getLocalizedPrice()
        ].compactMap { $0 }.joined(separator: " • ")
    }
}

struct ActivityDetailGetAccessSection_Previews: PreviewProvider {
    static var previews: some View {
        ActivityDetailGetAccessSection(viewModel: ActivityDetailView.ViewModel(nil))
    }
}
