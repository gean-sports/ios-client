//
//  ActivityDetailQRCodeSection.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/21/22.
//

import SwiftUI

struct ActivityDetailQRCodeSection: View {

    // MARK: - Variables

    @ObservedObject var viewModel: ActivityDetailView.ViewModel

    @State private var qrCodeImage: UIImage?

    // MARK: - Body

    var body: some View {

        VStack(alignment: .leading, spacing: 15) {

            VStack(alignment: .leading, spacing: 5) {

                Text("Your QR code")
                    .font(.system(size: 25, weight: .bold))
                    .foregroundColor(Color.Interface.black)

                if let validUntilText = generateValidUntilText() {

                    Text(validUntilText)
                        .font(.system(size: 16, weight: .bold))
                        .foregroundColor(Color.Interface.platinum)
                        .lineLimit(1)
                }
            }

            VStack {

                Image(uiImage: qrCodeImage ?? UIImage())
                    .interpolation(.none)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 250, height: 250)
                    .background(Color.Interface.cultured)
                    .task {

                        qrCodeImage = generateQRCodeImage(
                            from: viewModel.activityPass?.getQRCodeValue()
                        )
                    }
            }
            .frame(maxWidth: .infinity)
        }
    }

    // MARK: - Methods

    private func generateValidUntilText() -> String? {

        guard let endDate = viewModel.activityPass?.getEndDate()
        else { return nil }

        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium

        return [
            "Valid until",
            dateFormatter.string(from: endDate)
        ].compactMap { $0 }.joined(separator: " ")
    }

    private func generateQRCodeImage(from string: String?) -> UIImage? {

        guard let string = string
        else { return nil }

        let filter = CIFilter.qrCodeGenerator()
        filter.message = Data(string.utf8)

        let context = CIContext()

        guard let outputImage = filter.outputImage,
              let cgImage = context.createCGImage(outputImage, from: outputImage.extent)
        else { return nil }

        return UIImage(cgImage: cgImage)
    }
}

struct ActivityDetailQRCodeSection_Previews: PreviewProvider {
    static var previews: some View {
        ActivityDetailQRCodeSection(viewModel: ActivityDetailView.ViewModel(nil))
    }
}
