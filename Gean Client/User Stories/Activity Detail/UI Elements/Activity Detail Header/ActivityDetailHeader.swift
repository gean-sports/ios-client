//
//  ActivityDetailHeader.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/21/22.
//

import SwiftUI
import NukeUI

struct ActivityDetailHeader: View {

    // MARK: - Variables

    @ObservedObject var viewModel: ActivityDetailView.ViewModel

    // MARK: - Body

    var body: some View {

        ZStack(alignment: .bottomLeading) {

            LazyImage(
                source: viewModel.activity?.getBackgroundImageUrl(),
                resizingMode: .aspectFill
            )

            Color.Interface.black.opacity(0.5)

            VStack(
                alignment: .leading,
                spacing: 5
            ) {

                if let mainText = viewModel.activity?.getName() {

                    Text(mainText)
                        .foregroundColor(Color.Interface.white)
                        .font(.system(size: 20, weight: .bold))
                        .lineLimit(1)
                }

                Color.Interface.white
                    .frame(width: 60, height: 3)

                if
                    let activity = viewModel.activity,
                    let secondaryText = activity.getLocalizedDate() ?? activity.getAddress()?.getDescription()
                {

                    Text(secondaryText)
                        .foregroundColor(Color.Interface.platinum)
                        .font(.system(size: 16, weight: .medium))
                        .padding(.top, 5)
                        .lineLimit(1)
                }
            }
            .padding(20)
        }
    }
}

struct ActivityDetailHeader_Previews: PreviewProvider {
    static var previews: some View {
        ActivityDetailHeader(viewModel: ActivityDetailView.ViewModel(nil))
    }
}
