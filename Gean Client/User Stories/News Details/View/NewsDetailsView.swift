//
//  NewsDetailsView.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/22/22.
//

import SwiftUI
import NukeUI

struct NewsDetailsView: View {

    // MARK: - Variables

    var news: News

    // MARK: - Body

    var body: some View {

        ScrollView(
            .vertical,
            showsIndicators: false
        ) {

            VStack(alignment: .leading, spacing: 15) {

                if let backgroundImageUrl = news.getBackgroundImageUrl()?.absoluteString {

                    LazyImage(
                        source: backgroundImageUrl,
                        resizingMode: .aspectFill
                    )
                    .frame(height: 250)
                    .cornerRadius(5)
                }

                VStack(alignment: .leading, spacing: 5) {

                    Text(getNewsTitle())
                        .font(.system(size: 20, weight: .bold))
                        .foregroundColor(Color.Interface.black)

                    Text(getUpdateAtText())
                        .font(.system(size: 16, weight: .bold))
                        .foregroundColor(Color.Interface.platinum)
                }
                
                if let description = news.getDescription() {
                    
                    Text(description)
                        .font(.system(size: 16, weight: .medium))
                        .foregroundColor(Color.Interface.black)
                }
            }
        }
        .padding(20)
    }

    // MARK: - Interaction Methods

    private func getNewsTitle() -> String {

        return news.getTitle() ?? ""
    }

    private func getUpdateAtText() -> String {

        let date = news.getCreatedAt() ?? Date()

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        let dateText = dateFormatter.string(from: date)

        return String(
            format: "Published on %@",
            dateText
        )
    }
}

struct NewsDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        NewsDetailsView(news: News())
    }
}
