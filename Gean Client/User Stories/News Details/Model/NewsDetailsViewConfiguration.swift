//
//  NewsDetailsViewConfiguration.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/22/22.
//

import Foundation

struct NewsDetailsViewConfiguration: Identifiable {

    // MARK: - Variables

    var id: Int?

    var news: News
}
