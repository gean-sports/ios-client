//
//  SearchView.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/15/22.
//

import SwiftUI
import SwiftUIPullToRefresh

struct SearchView: View {

    // MARK: - Configuration Variables

    private var configuration: SearchViewConfiguration {

        viewModel.configuration
    }

    // MARK: - Variables

    @StateObject var viewModel: SearchView.ViewModel

    @Environment(\.presentationMode) var presentationMode

    @EnvironmentObject private var mainTabCoordinator: MainTabCoordinator

    

    @State var isShowingActivityDetailViewWithConfiguration: ActivityDetailViewConfiguration?

    // MARK: - Body

    var body: some View {

        VStack {

            if configuration.isShowingSearchBar {

                SearchTextFieldView(
                    shouldShowLocationButton: configuration.isLocationEnabled,
                    scope: configuration.scope,
                    searchQuery: $viewModel.searchQuery
                )
                .padding(.top, 20)
                .padding([.horizontal], 20)
            }

            RefreshableScrollView(showsIndicators: false) {

                await viewModel.refreshActivityPasses()
            } progress: { state in

                RefreshActivityIndicator(isAnimating: true)
            } content: {

                LazyVStack(
                    alignment: .leading,
                    spacing: 0
                ) {

                    if !viewModel.fitnessCenterList.isEmpty {

                        Text("Fitness centers")
                            .font(.system(size: 25, weight: .bold))
                            .padding(.top, 20)
                            .padding(.bottom, 5)

                        ForEach(viewModel.fitnessCenterList) { fitnessCenter in

                            SearchItemCell(activity: fitnessCenter)
                                .onTapGesture {

                                    if presentationMode.wrappedValue.isPresented {

                                        isShowingActivityDetailViewWithConfiguration = ActivityDetailViewConfiguration(activity: fitnessCenter)
                                    } else {

                                        mainTabCoordinator.isShowingActivityDetailViewWithConfiguration = ActivityDetailViewConfiguration(activity: fitnessCenter)
                                    }
                                }
                        }
                    }

                    if !viewModel.eventList.isEmpty {

                        Text("Events")
                            .font(.system(size: 25, weight: .bold))
                            .padding(.top, 20)
                            .padding(.bottom, 5)

                        ForEach(viewModel.eventList) { event in

                            SearchItemCell(activity: event)
                                .onTapGesture {

                                    if presentationMode.wrappedValue.isPresented {

                                        isShowingActivityDetailViewWithConfiguration = ActivityDetailViewConfiguration(activity: event)
                                    } else {

                                        mainTabCoordinator.isShowingActivityDetailViewWithConfiguration = ActivityDetailViewConfiguration(activity: event)
                                    }
                                }
                        }
                    }
                }
                .animation(.easeInOut(duration: 0.15), value: viewModel.fitnessCenterList.count)
                .animation(.easeInOut(duration: 0.15), value: viewModel.eventList.count)
                .padding(.horizontal, 20)
                .padding(.bottom, configuration.isShowingTabBarPadding ? 80 : 0)
            }
        }
        .background(Color.Interface.white)
        .sheet(
            item: $isShowingActivityDetailViewWithConfiguration,
            onDismiss: { isShowingActivityDetailViewWithConfiguration = nil },
            content: { configuration in

                ActivityDetailView(
                    viewModel: ActivityDetailView.ViewModel(
                        configuration.activity
                    )
                )
            }
        )
    }
}

struct SearchView_Previews: PreviewProvider {
    static var previews: some View {
        SearchView(viewModel: SearchView.ViewModel(configuration: SearchViewConfiguration()))
    }
}
