//
//  SearchTextFieldView.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/15/22.
//

import SwiftUI
import CoreLocationUI

struct SearchTextFieldView: View {
    
    // MARK: - Configuration Variables
    
    var shouldShowLocationButton: Bool = true
    
    var scope = [SearchScope]()

    // MARK: - Variables

    @Binding var searchQuery: String

    @FocusState private var isTextFieldFocused: Bool
    
    // MARK: - Body

    var body: some View {

        HStack(spacing: 10) {

            HStack(spacing: 10) {

                Image(systemName: "magnifyingglass")
                    .resizable()
                    .frame(width: 20, height: 20)
                    .foregroundColor(isTextFieldFocused || !searchQuery.isEmpty ? Color.Interface.black : Color.Interface.platinum)
                    .animation(.easeIn(duration: 0.1), value: isTextFieldFocused)

                ZStack(alignment: .leading) {
                    
                    let placeholderText = String(
                        format: "Search for %@",
                        scope.map { $0.getTextFieldPlaceholder() }
                            .uniqued()
                            .joined(separator: ", ")
                    )

                    Text(placeholderText)
                        .font(.system(size: 14, weight: .bold))
                        .lineLimit(1)
                        .opacity(searchQuery.isEmpty ? 1 : 0)
                        .foregroundColor(Color.Interface.platinum)
                        .animation(.easeInOut(duration: 0.1), value: searchQuery)

                    TextField("", text: $searchQuery)
                        .tint(Color.Interface.black)
                        .accentColor(Color.Interface.black)
                        .font(.system(size: 14, weight: .bold))
                        .focused($isTextFieldFocused)
                        .disableAutocorrection(true)
                        .textInputAutocapitalization(.words)
                        .frame(height: 50)
                }
            }
            .padding(.leading, 15)
            .padding(.trailing, 10)
            .background(Color.Interface.cultured)
            .onTapGesture {

                isTextFieldFocused = true
            }
            
            /*if shouldShowLocationButton {
            
                LocationButton {
                    print(9999)
                }
                .symbolVariant(.fill)
                .labelStyle(.iconOnly)
                .foregroundColor(.white)
                .clipShape(Capsule())
            }*/
        }
    }
}

struct SearchTextFieldView_Previews: PreviewProvider {
    
    static var previews: some View {
        SearchTextFieldView(searchQuery: .constant(""))
    }
}
