//
//  SearchItemCell.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/15/22.
//

import SwiftUI
import NukeUI

struct SearchItemCell: View {

    // MARK: - Variables

    var activity: Activity

    // MARK: - Body

    var body: some View {
        

        VStack(
            alignment: .leading,
            spacing: 10
        ) {

            HStack(
                alignment: .center,
                spacing: 15) {

                    LazyImage(
                        source: activity.getBackgroundImageUrl(),
                        resizingMode: .aspectFill
                    )
                    .frame(width: 70, height: 70)

                    VStack(
                        alignment: .leading,
                        spacing: 1
                    ) {

                        if let mainText = activity.getName() {

                            Text(mainText)
                                .foregroundColor(Color.Interface.black)
                                .font(.system(size: 20, weight: .bold))
                                .lineLimit(1)
                        }

                        Color.Interface.black
                            .frame(width: 60, height: 3)

                        if let secondaryText = activity.getLocalizedDate() ?? activity.getAddress()?.getDescription()
                        {

                            Text(secondaryText)
                                .foregroundColor(Color.Interface.platinum)
                                .font(.system(size: 16, weight: .medium))
                                .padding(.top, 10)
                                .lineLimit(1)
                        }
                    }

                    Spacer()

                    Image(systemName: "arrow.right.circle.fill")
                        .resizable()
                        .frame(width: 30, height: 30)
                }

            Divider()
                .background(Color.Interface.platinum)
        }
        .padding(.top, 10)
    }
}

struct SearchItemCell_Previews: PreviewProvider {
    static var previews: some View {
        SearchItemCell(activity: FitnessCenter())
    }
}
