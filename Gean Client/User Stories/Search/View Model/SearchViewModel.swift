//
//  SearchViewModel.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 6/4/22.
//

import Foundation
import Combine

extension SearchView {

    final class ViewModel: ObservableObject {

        // MARK: - Variables

        var configuration: SearchViewConfiguration

        @Published var fitnessCenterList = [FitnessCenter]()

        @Published var eventList = [Event]()

        @Published var searchQuery: String = ""

        private var cancellables = Set<AnyCancellable>()

        init(configuration: SearchViewConfiguration) {

            self.configuration = configuration

            bindSearchQuery()

            bindActivityPassList()
        }

        // MARK: - Interaction Methods

        public func refreshActivityPasses() async {

            try? await AccountService.shared.refreshActivityPasses()

            await refreshBindingActivityList()
        }

        public func fetchActivityList(with searchQuery: String) {

            Task(priority: .userInitiated) {

                async let getFitnessCenters = try? NetworkService.getFitnessCenters(
                    searchQuery: searchQuery,
                    limit: 3
                )

                async let getEvents = try? NetworkService.getEvents(
                    searchQuery: searchQuery,
                    limit: 3
                )

                let (fitnessCenterList, eventList) = await (getFitnessCenters, getEvents)

                await MainActor.run {

                    self.fitnessCenterList = fitnessCenterList ?? []

                    self.eventList = eventList ?? []
                }
            }
        }

        // MARK: - Methods

        @MainActor
        private func refreshBindingActivityList() async {

            if configuration.scope.contains(.userFitnessCenters) {

                fitnessCenterList = AccountService.shared.activityPassList?
                    .compactMap { $0.getFitnessCenter() }
                    .sorted {

                        guard let lhsName = $0.getName(),
                              let rhsName = $1.getName()
                        else { return false }

                        return lhsName < rhsName
                    } ?? []
            }

            if configuration.scope.contains(.userEvents) {

                eventList = AccountService.shared.activityPassList?
                    .compactMap { $0.getEvent() }
                    .sorted {

                        guard let lhsDate = $0.getDate(),
                              let rhsDate = $1.getDate()
                        else { return false }

                        return lhsDate < rhsDate
                    } ?? []
            }
        }

        private func bindSearchQuery() {

            $searchQuery
                .debounce(for: 0.3, scheduler: RunLoop.main)
                .sink { [weak self] searchQuery in

                    guard let self = self
                    else { return }

                    if searchQuery.isEmpty {

                        if self.configuration.scope.contains(.fitnessCenters) {

                            self.fitnessCenterList = []
                        }

                        if self.configuration.scope.contains(.events) {

                            self.eventList = []
                        }
                    } else {

                        self.fetchActivityList(with: searchQuery)
                    }
                }
                .store(in: &cancellables)
        }

        private func bindActivityPassList() {

            AccountService.shared.$activityPassList.sink { [weak self] _ in

                Task { [weak self] in

                    guard let self = self
                    else { return }

                    await self.refreshBindingActivityList()
                }
            }.store(in: &cancellables)
        }
    }
}
