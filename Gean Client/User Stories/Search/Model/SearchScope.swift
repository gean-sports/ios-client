//
//  SearchScope.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/19/22.
//

import Foundation

enum SearchScope: Identifiable {

    // MARK: - Cases

    case events
    case fitnessCenters

    case userEvents
    case userFitnessCenters

    // MARK: - Identifiable

    var id: Int { hashValue }

    // MARK: - Interaction Methods

    func getTextFieldPlaceholder() -> String {

        switch self {
        case .events, .userEvents:
            
            return "events"
        case .fitnessCenters, .userFitnessCenters:
            
            return "fitness centers"
        }
    }
}
