//
//  SearchViewConfiguration.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/19/22.
//

import Foundation

struct SearchViewConfiguration: Identifiable {

    var id: Int { 0 }

    // MARK: - Variables

    var scope = [SearchScope]()
    
    var isShowingSearchBar = true
    
    var isLocationEnabled = true
    
    var isShowingTabBarPadding = false
}
