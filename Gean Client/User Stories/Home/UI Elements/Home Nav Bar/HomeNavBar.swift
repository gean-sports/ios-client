//
//  HomeNavBar.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/15/22.
//

import SwiftUI

struct HomeNavBar: View {

    // MARK: - Variables
    
    @EnvironmentObject private var mainTabCoordinator: MainTabCoordinator

    @StateObject var accountService = AccountService.shared

    // MARK: - Body

    var body: some View {

        HStack(alignment: .center, spacing: 20) {

            /*Button {

                mainTabCoordinator.isShowingSearchViewWithConfiguration = SearchViewConfiguration(scope: [.userFitnessCenters, .fitnessCenters])
            } label: {

                HStack(alignment: .center, spacing: 15) {

                    Image(systemName: "building.2.fill")
                        .resizable()
                        .scaledToFit()
                        .frame(width: 20, height: 20)
                        .padding(10)
                        .tint(Color.Interface.black)
                        .background(Color.Interface.platinum)
                        .clipShape(Circle())

                    VStack(alignment: .leading, spacing: 2) {

                        Text("Nearest fitness center")
                            .font(.system(size: 16, weight: .bold))
                            .tint(Color.Interface.black)
                            .lineLimit(1)

                        Text("Tap to select your fitness center")
                            .font(.system(size: 14, weight: .medium))
                            .tint(Color.Interface.platinum)
                            .lineLimit(1)
                    }
                }
            }*/

            Text("Gean Sports")
                .font(.system(size: 20, weight: .bold, design: .rounded))
                .tint(Color.Interface.black)
                .lineLimit(1)
                .frame(height: 40)

            Spacer()

            if accountService.authStatus == .authenticated {

                Menu {

                    Button("Sign out") {

                        try? AccountService.shared.signOut()
                    }
                } label: {

                    Image(systemName: "person.crop.circle.fill")
                        .resizable()
                        .scaledToFit()
                        .frame(width: 35, height: 35)
                        .tint(Color.Interface.black)
                }
            } else {

                Button {

                    mainTabCoordinator.isShowingSignInView = true
                } label: {

                    Image(systemName: "person.crop.circle.fill.badge.plus")
                        .resizable()
                        .scaledToFit()
                        .frame(width: 40, height: 40)
                        .tint(Color.Interface.black)
                }
            }
        }
    }
}


struct HomeNavBar_Previews: PreviewProvider {
    static var previews: some View {
        HomeNavBar()
    }
}
