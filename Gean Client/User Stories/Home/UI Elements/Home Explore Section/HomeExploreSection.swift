//
//  HomeExploreSection.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/15/22.
//

import SwiftUI
import NukeUI

struct HomeExploreSection: View {

    // MARK: - Variables

    @EnvironmentObject private var mainTabCoordinator: MainTabCoordinator
    
    // MARK: - Body

    var body: some View {

        VStack(alignment: .leading, spacing: 15) {

            Text("Explore 🌁")
                .font(.system(size: 25, weight: .bold))
                .foregroundColor(Color.Interface.black)
                .lineLimit(1)

            Button {
                
                mainTabCoordinator.isShowingSearchViewWithConfiguration = SearchViewConfiguration(
                    scope: [.fitnessCenters, .events]
                )
            } label: {
                
                SearchTextFieldView(
                    shouldShowLocationButton: false,
                    scope: [.fitnessCenters, .events],
                    searchQuery: .constant("")
                )
                .allowsHitTesting(false)
            }
        }
        .padding(.horizontal, 20)
    }
}

struct HomeExploreSection_Previews: PreviewProvider {
    static var previews: some View {
        HomeExploreSection()
    }
}
