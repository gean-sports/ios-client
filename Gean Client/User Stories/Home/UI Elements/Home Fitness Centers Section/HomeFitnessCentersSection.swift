//
//  HomeFitnessCentersSection.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/15/22.
//

import SwiftUI

struct HomeFitnessCentersSection: View {

    // MARK: - Variables

    @EnvironmentObject private var mainTabCoordinator: MainTabCoordinator

    var fitnessCenters: [FitnessCenter]

    // MARK: - Body

    var body: some View {

        VStack(alignment: .leading, spacing: 15) {

            Text("Our fitness centers 🌍")
                .font(.system(size: 25, weight: .bold))
                .foregroundColor(Color.Interface.black)
                .lineLimit(1)
            
            LazyVStack(spacing: 20) {
                
                ForEach(fitnessCenters) { fitnessCenter in
                    
                    HomeFitnessCentersSectionCell(fitnessCenter: fitnessCenter)
                        .onTapGesture {

                            let configuration = ActivityDetailViewConfiguration(activity: fitnessCenter)

                            mainTabCoordinator.isShowingActivityDetailViewWithConfiguration = configuration
                        }
                }
            }
        }
        .padding(.horizontal, 20)
    }
}

struct HomeFitnessCentersSection_Previews: PreviewProvider {
    static var previews: some View {
        HomeFitnessCentersSection(fitnessCenters: [])
    }
}
