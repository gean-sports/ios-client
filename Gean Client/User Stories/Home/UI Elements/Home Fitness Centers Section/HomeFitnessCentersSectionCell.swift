//
//  HomeFitnessCentersSectionCell.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/15/22.
//

import SwiftUI
import NukeUI

struct HomeFitnessCentersSectionCell: View {

    // MARK: - Variables

    var fitnessCenter: FitnessCenter

    // MARK: - Body

    var body: some View {

        ZStack(alignment: .bottomLeading) {

            LazyImage(
                source: fitnessCenter.getBackgroundImageUrl(),
                resizingMode: .aspectFill
            )
            
            Color.Interface.black.opacity(0.5)
            
            
            HStack(spacing: 10) {

                VStack(alignment: .leading, spacing: 10) {

                    if let name = fitnessCenter.getName() {

                        Text(name)
                            .foregroundColor(Color.Interface.white)
                            .font(.system(size: 20, weight: .bold))
                    }

                    Divider()
                        .frame(height: 3)
                        .frame(maxWidth: 60)
                        .background(Color.Interface.white)

                    if let addressDescription = fitnessCenter.getAddress()?.getDescription() {

                        Text(addressDescription)
                            .foregroundColor(Color.Interface.platinum)
                            .font(.system(size: 16, weight: .semibold))
                    }
                }
                .lineLimit(1)
                
                Spacer()
                
                Image(systemName: "arrow.right.circle.fill")
                    .resizable()
                    .foregroundColor(.white)
                    .frame(width: 30, height: 30)
            }
            .padding(.vertical, 15)
            .padding(.horizontal, 10)
        }
        .cornerRadius(5)
        .frame(height: 130)
    }
}

struct HomeFitnessCentersSectionCell_Previews: PreviewProvider {
    static var previews: some View {
        HomeFitnessCentersSectionCell(fitnessCenter: FitnessCenter())
    }
}
