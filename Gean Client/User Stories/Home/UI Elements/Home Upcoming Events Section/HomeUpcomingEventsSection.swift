//
//  HomeUpcomingEventsSection.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/15/22.
//

import SwiftUI

struct HomeUpcomingEventsSection: View {

    // MARK: - Variables

    @EnvironmentObject private var mainTabCoordinator: MainTabCoordinator

    var events: [Event]

    // MARK: - Body

    var body: some View {

        VStack(alignment: .leading, spacing: 15) {

            Text("Upcoming events 😍")
                .font(.system(size: 25, weight: .bold))
                .foregroundColor(Color.Interface.black)
                .lineLimit(1)
                .padding(.horizontal, 20)

            ScrollView(.horizontal, showsIndicators: false) {

                LazyHStack(spacing: 20) {

                    ForEach(events) { event in

                        HomeUpcomingEventsSectionCell(event: event)
                            .onTapGesture {

                                let configuration = ActivityDetailViewConfiguration(activity: event)

                                mainTabCoordinator.isShowingActivityDetailViewWithConfiguration = configuration
                            }
                    }
                }
                .padding(.horizontal, 20)
            }
        }
        .frame(maxWidth: .infinity, alignment: .leading)
        .padding(.vertical, 20)
        .background(Color.Interface.cultured)
    }
}

struct HomeUpcomingEventsSection_Previews: PreviewProvider {
    static var previews: some View {
        HomeUpcomingEventsSection(events: [])
    }
}
