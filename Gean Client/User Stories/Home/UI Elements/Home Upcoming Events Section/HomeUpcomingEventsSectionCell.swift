//
//  HomeUpcomingEventsCellSection.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/15/22.
//

import SwiftUI
import NukeUI

struct HomeUpcomingEventsSectionCell: View {

    // MARK: - Variables

    var event: Event

    // MARK: - Body

    var body: some View {

        ZStack(alignment: .bottomLeading) {

            LazyImage(
                source: event.getBackgroundImageUrl(),
                resizingMode: .aspectFill
            )

            Color.black.opacity(0.5)

            VStack(alignment: .leading, spacing: 10) {

                if let name = event.getName() {

                    Text(name)
                        .foregroundColor(Color.Interface.white)
                        .font(.system(size: 20, weight: .bold))
                }

                Divider()
                    .frame(height: 3)
                    .frame(maxWidth: 60)
                    .background(Color.Interface.white)

                if let localizedDate = event.getLocalizedDate() {

                    Text(localizedDate)
                        .foregroundColor(Color.Interface.platinum)
                        .font(.system(size: 16, weight: .bold))
                }
            }
            .padding(.vertical, 15)
            .padding(.horizontal, 10)
        }
        .frame(width: 140, height: 215)
        .cornerRadius(5)
    }
}

struct HomeUpcomingEventsSectionCell_Previews: PreviewProvider {
    static var previews: some View {
        HomeUpcomingEventsSectionCell(event: Event())
    }
}
