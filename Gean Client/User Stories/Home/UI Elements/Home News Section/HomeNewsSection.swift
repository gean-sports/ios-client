//
//  HomeNewsSection.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/15/22.
//

import SwiftUI
import ASCollectionView

struct HomeNewsSection: View {

    // MARK: - Variables

    @EnvironmentObject private var mainTabCoordinator: MainTabCoordinator

    var news: [News]

    // MARK: - Body

    var body: some View {

        ASCollectionView(
            data: news,
            dataID: \.id
        ) { item, _ in

            HomeNewsCarouselCell(news: item)
                .onTapGesture {

                    mainTabCoordinator.isShowindNewsDetailsViewWithConfiguration = NewsDetailsViewConfiguration(news: item)
                }
        }
        .layout(layout: getCollectionLayout)
        .frame(height: 300)
    }

    // MARK: - Methods

    func getCollectionLayout() -> ASCollectionLayoutSection {

        return ASCollectionLayoutSection {

            let item = NSCollectionLayoutItem(
                layoutSize: NSCollectionLayoutSize(
                    widthDimension: .fractionalWidth(1.0),
                    heightDimension: .fractionalHeight(1.0)
                )
            )

            let group = NSCollectionLayoutGroup.vertical(
                layoutSize: NSCollectionLayoutSize(
                    widthDimension: .absolute(UIScreen.main.bounds.width - 40),
                    heightDimension: .fractionalHeight(1)
                ),
                subitems: [item]
            )

            let section = NSCollectionLayoutSection(group: group)
            section.orthogonalScrollingBehavior = .groupPaging
            section.interGroupSpacing = 15
            section.contentInsets = NSDirectionalEdgeInsets(
                top: 0,
                leading: 20,
                bottom: 0,
                trailing: 20
            )

            return section
        }
    }
}

struct HomeNewsSection_Previews: PreviewProvider {
    static var previews: some View {
        HomeNewsSection(news: [])
            .environmentObject(MainTabCoordinator())
    }
}
