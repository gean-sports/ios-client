//
//  HomeNewsCarouselCell.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/15/22.
//

import SwiftUI
import NukeUI

struct HomeNewsCarouselCell: View {

    // MARK: - Variables

    var news: News

    // MARK: - Body

    var body: some View {

        VStack(alignment: .leading, spacing: 15) {

            VStack(alignment: .leading, spacing: 5) {

                Text(getNewsTitle())
                    .font(.system(size: 20, weight: .bold))
                    .foregroundColor(Color.Interface.black)
                    .lineLimit(1)

                Text(getUpdateAtText())
                    .font(.system(size: 16, weight: .bold))
                    .foregroundColor(Color.Interface.platinum)
                    .lineLimit(1)
            }

            LazyImage(
                source: news.getBackgroundImageUrl()?.absoluteString,
                resizingMode: .aspectFill
            )
            .cornerRadius(5)
        }
    }

    // MARK: - Interaction Methods

    private func getNewsTitle() -> String {

        return news.getTitle() ?? ""
    }

    private func getUpdateAtText() -> String {

        let date = news.getCreatedAt() ?? Date()

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        let dateText = dateFormatter.string(from: date)

        return String(
            format: "Published on %@",
            dateText
        )
    }
}

struct HomeNewsCarouselCell_Previews: PreviewProvider {
    static var previews: some View {
        HomeNewsCarouselCell(news: News())
            .frame(height: 300)
    }
}
