//
//  HomeView.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/8/22.
//

import SwiftUI
import SwiftUIPullToRefresh

struct HomeView: View {

    // MARK: - Variables

    @StateObject var viewModel = ViewModel()

    // MARK: - Body
    
    var body: some View {

        VStack() {
            
            HomeNavBar()
                .padding(.horizontal, 20)
                .padding(.top, 10)
            
            Divider()
                .background(Color.Interface.platinum)
                .padding(.horizontal, 20)

            RefreshableScrollView(showsIndicators: false) {

                await viewModel.refreshNews()
            } progress: { state in

                RefreshActivityIndicator(isAnimating: [.primed, .loading, .waiting].contains(state))
            } content: {

                LazyVStack(alignment: .leading, spacing: 25) {

                    if !viewModel.news.isEmpty {

                        HomeNewsSection(news: viewModel.news)
                    }

                    HomeExploreSection()

                    if !viewModel.events.isEmpty {

                        HomeUpcomingEventsSection(events: viewModel.events)
                    }

                    if !viewModel.fitnessCenters.isEmpty {

                        HomeFitnessCentersSection(fitnessCenters: viewModel.fitnessCenters)
                    }
                }
                .animation(
                    .easeInOut,
                    value: viewModel.news.count +
                        viewModel.events.count +
                        viewModel.fitnessCenters.count
                )
                .padding(.top, 25)
                .padding(.bottom, 80)
            }
        }
        .background(Color.Interface.white)
        .onTapGesture(perform: hideKeyboard)
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
