//
//  HomeViewModel.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 5/22/22.
//

import Foundation

extension HomeView {

    @MainActor
    final class ViewModel: ObservableObject {

        // MARK: - Variables

        @Published var news = [News]()

        @Published var events = [Event]()

        @Published var fitnessCenters = [FitnessCenter]()

        // MARK: - Init

        init() {

            Task(priority: .background) {

                await refreshNews()
            }
        }

        // MARK: - Interaction Methods

        public func refreshNews() async {

            async let newsRequest = NetworkService.getNews()
            async let eventsRequest = NetworkService.getEvents()
            async let fitnessCentersRequest = NetworkService.getFitnessCenters()

            let (retrievedNews, retrievedEvents, retrievedFitnessCenters) = try! await (newsRequest, eventsRequest, fitnessCentersRequest)

            news = retrievedNews

            events = retrievedEvents

            fitnessCenters = retrievedFitnessCenters
        }
    }
}
