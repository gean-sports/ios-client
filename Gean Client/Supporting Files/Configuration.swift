//
//  Configuration.swift
//  Gean Client
//
//  Created by Vlad Gordiichuk on 6/4/22.
//

import Foundation

enum Configuration {

    // MARK: - Services

    enum Services {

        // MARK: - Stripe

        enum Stripe {

            static let merchantIdentifier = "merchant.com.eltex.gean-sports-client.apple-pay"
        }
    }
}
